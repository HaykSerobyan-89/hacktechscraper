from bs4 import BeautifulSoup
import requests
import csv


def get_movie_info(url='https://www.imdb.com/chart/top'):
    resp_text = requests.get(url).text
    soup = BeautifulSoup(resp_text, "html.parser")
    return zip(soup.findAll("td", class_="titleColumn"),
               soup.findAll("td", class_="ratingColumn imdbRating"))


def write_data_csv(data):
    with open('films.csv', 'w', encoding='UTF8') as file:
        writer = csv.writer(file)
        writer.writerow(["Rank", "Title", "Year", "Rating"])
        for film, rating in data:
            rank, *title, year = film.text.strip().split()
            writer.writerow([rank[:-1], " ".join(title), year, rating.text.strip()])


if __name__ == "__main__":
    movie_data = get_movie_info()
    write_data_csv(movie_data)
